package controllers;

import java.text.SimpleDateFormat;
import java.util.HashMap;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;

import models.Agent;
import models.Compte;
import models._Agent_decoration;
import play.data.validation.Required;
import play.i18n.Messages;

public class Comptes  extends AppController{
	//Agent agent = null;
	  public static void login() {
	    	render();
	    }
	 
	  public static void dashbord() {
	    	render();
	    }
	  
	  public static void motdepasse() {
	    	render();
	    }
	  public static void reinitialisation() {
	    	render();
	    } 
	  
	  

      public static void edits(long id) {
    if (id > 0) {
       Agent  agent = Agent.findById(id);
        Compte object =  new Compte(); 
        if (agent != null) { 
     renderTemplate("Comptes/edit.html", agent,object);
        }
    }
    flash.error("object.not_found");
    list(null, null, null, 1);
}
	    public static void edit(Long id) {
	        if (id != null) {
	        	Compte object = Compte.findById(id);
	            Agent agent  = object.getAgent();
	            System.out.println(agent);
	            if (object != null) {	         	 
	                render(object,agent);
	            } else {
	                flash.error("object.not_found");
	                list(null, null, null, 1);
	            }
	        }
	        render();
	    }
	  
	  /*
       * enregistrement des droit d un utilisateur
       *  */
             public static void agentdroitSave(Compte object, Agent agent,String type_droit,String login,String password,String type_cpte ){
                 if (object != null) {
                 	object.setAgent(agent);
                 	object.setLogin(login);
                 	object.setMot_passe(password);
                 	object.setTypecpte(type_cpte);
                 	if (object.validateAndSave()){
                 		flash.success("compte.saved.success");
                 		 list(null,null,null,1);
                 	}
                 	else
                 	{
                 		flash.error("compte.saved.error");
             	          renderTemplate("Comptes/edit.html");
                 		
                 	}
                 	}
                 else{
                 	flash.error("compte.saved.error");
       	          renderTemplate("Agents/edit.html");
                 }
                 	
                 	}
	 
	  public static void authenticate(@Required String login, @Required String mot_passe) {
	        checkAuthenticity();
	  
	       	if( !login.equalsIgnoreCase("") &&  !mot_passe.equalsIgnoreCase("") )
	       	{
	        Compte user = Compte.find("byLogin",login).first();
	        
	        if (user == null) {
	           	 
             // flash.error(Messages.get("auth.error"));
	            flash.error("Verifier vos parametres de connexion");
	            renderArgs.put("login", login);
	           // renderTemplate("Comptes/login.html");
	            Application.login();
	        
	        } else {
	        	Agent agent = user.getAgent();
	        	System.out.println(user.getAgent().getId().toString());
	        	
	        	if(user.getTypecpte().equalsIgnoreCase("1")){
	        		
	        		 System.out.println("Vous etes un utlisateur");	
	        	     session.put("user",user.getAgent().getId());
	        	  	 renderTemplate("Client.html",agent);
	        	  	 
	        	}
	        	else if(user.getTypecpte().equalsIgnoreCase("2")){
	        		System.out.println("Vous etes un administrateur");
	        		if (session.isEmpty()){
	        			 session.put("admin",user.getAgent().getId());
	        		}else{
	        			session.remove("user");
	        			session.put("admin",user.getAgent().getId());
	        		}
	            flash.success("Bienvenue");
	//            response.setCookie("playlonglivecookie",user.getAgent().getId().toString(), "admin");
	        
	           	 renderTemplate("stat.html",agent);
	        	}
	        }
	        }
	        
	        else{
	        	System.out.println("Renseignez les champs");	
	        }
	    }
	  public static void view(long id) {
	        if (id > 0) {
	        	Compte object = Compte.findById(id);
	            if (object != null) {
	            	Agent agent = object.getAgent();
	                render(agent,object);
	            }
	        }
	        flash.error("object.not_found");
	        list(null, null, null, 1);
	    }
	  
      public static void recherche_agent_cpte(String k, String sF, String sD, int page) {
      HashMap<String, Object> result =Agent.search(k, sF, sD, page);
      render(result, k, sF, sD, page);

  }
  	public static void delete(long id) {
        checkAuthenticity();
        Compte object = Compte.findById(id);
        if (object != null) {
            try {
            	object.delete();
                flash.success("agent.deleted.success");
                list(null, null, null, 1);
            } catch (Exception e) {
                flash.error(Messages.get("agent_has_children.error"));
                view(id);
            }
        } else {
            flash.error("object.deleted.error");
            list(null, null, null, 1);
        }
    }
      
	  
	  public static void list(String k, String sF, String sD, int page) {
	        HashMap<String, Object> result = Compte.search(k, sF, sD, page);
	        render(result, k, sF, sD, page);
	    }
}
