package controllers;

import play.*;
import play.data.validation.Required;
import play.i18n.Messages;
import play.mvc.*;

import java.util.*;

import models.*;

public class Application extends Controller {
	
	 public static void content() {
		 renderTemplate("stat.html");
	    }
	 
	 public static void param() {
		 renderTemplate("param.html");
	    }
	 public static void login() {
		 renderTemplate("Application/login.html");
	    }

	 public static void wizard() {
		 renderTemplate("wizard.html");
	    }

    public static void index() {
    	Application.login();
    }
    
    public static void update() {
    	Application.login();
    }
    
  
    public static void client() {
   	 renderTemplate("Client.html");
    }
    
}